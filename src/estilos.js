/* eslint-disable no-unused-vars */
import { css, unsafeCSS } from 'lit-element';

export default css`:

.caja{
  display:flex;
  flex-flow: row wrap;
  justify-content:center;
}
.a_100{
  width:100%;
}
.a_90{
  width:90%;
}

.a_80{
  width:80%;
}
.caja2{
  display:flex;
  flex-flow: row wrap;
  justify-content:center;
  background-color:green;
}

.caja_vertical{
  display:flex;
  flex-flow: column wrap;
  align-items:center;
 
  
}

.red{
  background-color:red;
}

.blue{
  background: linear-gradient(to bottom, rgb(26,121,207), rgb(3,50,98));
}

.azul{
  background-color:rgb(3,50,98);
}

bbva-web-form-password.rojoo{
  --_field-bg-color: rgb(0,68,120);
  --_field-input-color:white;
  --_field-label-color:white;
  --_field-readonly-input-color:white;
  --_field-clear-color:white;
  --_field-focused-label-color:white;
}
.center {
  position: absolute;
  top: 15px;
  left: 50%;
  transform: translate(-50%, -50%);
  font-size: 15px;
  color :white;
}
.topleft {
  position: absolute;
  top: 8px;
  left: 10px;
  font-size: 12px;
  color :white;
}
.topright {
  position: absolute;
  top: 8px;
  right: 10px;
  font-size: 12px;
  color :white;
}
.bottom {
  position: absolute;
  bottom: 8px;
  font-size: 15px;
  color :white;
}
.circle{
  position:relative;
  top:-25px;
  border-radius:50%;
  width:50px;
  height:50px;
  background-color:rgb(0,68,120);
  color: white;
  display:flex;
  flex-flow:row wrap;
  justify-content:center;
  align-items:center;
}
.hidden{
  display:none;
}

.m_bt_5{
  margin: 5px 0px;
}



`;