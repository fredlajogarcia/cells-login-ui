import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './CellsLoginUi-styles.js';
import styles2 from './estilos.js';
import '@bbva-web-components/bbva-web-form-password/bbva-web-form-password.js';
import '@bbva-web-components/bbva-web-button-default/bbva-web-button-default.js';
import '@bbva-web-components/bbva-web-form-text/bbva-web-form-text.js';
import '@bbva-web-components/bbva-web-form-select/bbva-web-form-select.js';




/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-login-ui></cells-login-ui>
```

##styling-doc

@customElement cells-login-ui
*/
export class CellsLoginUi extends LitElement {
  static get is() {
    return 'cells-login-ui';
  }

  // Declare properties
  static get properties() {
    return {
      documento: { type: String, },
      user: { type: String, },
      contraseña: { type: String, },
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.documento = 'L';
    this.user = '31158005';
    this.password = '';
  }

  static get styles() {
    return [
      styles,styles2,
      getComponentSharedStyles('cells-login-ui-shared-styles,estilos')
    ];
  }

  // Define a template
  render() {
    return html`
      
      <div style="width:600px;" class="caja_vertical ">
          <div class="a_100">
          <img src="https://p0.pikist.com/photos/136/308/panoramic-sunset-dawn-nature-landscape-panorama-sky-travel-outdoors.jpg" alt="dqefecto" width="100%">
          </div>
          <div class="circle" >FK</div>
          
          <p style="color:white; margin-top:0px;margin-bottom:5px;">Hola, Fred Kevin</p>
          <p style="color:white; margin:5px;font-size: 12px;margin-bottom:15px;" @click=${this.usuario}>Cambiar Usuario</p>
        
          
          <div class="a_100 hidden caja_vertical m_bt_5" id="usuario">
            <bbva-web-form-select class="a_90 m_bt_5" label="Tipo de Documento" @change=${this.setTipoDocu}>
              <bbva-web-form-option value="L">DNI</bbva-web-form-option>
              <bbva-web-form-option value="R">RUC</bbva-web-form-option>
              <bbva-web-form-option value="E">CARNE EXTRANJERIA</bbva-web-form-option>
            </bbva-web-form-select>

            <bbva-web-form-text class="a_90 m_bt_5" label="Documento" value="31158005" @change=${this.setUser}></bbva-web-form-text>

          </div>  
        
          <div class="a_90">
            <bbva-web-form-password class ="rojoo" id="contra"  label="Ingresar Contraseña" @change=${this.setPassword} ></bbva-web-form-password>
          </div>
          <p style="color:white; margin:10px;font-size: 12px;">¿Olvidaste tu contraseña?</p>

          <bbva-web-button-default style="margin:10px; " @click=${this.login} >Log in</bbva-web-button-default>
          
      </div>
    `;
    
  }
  setPassword(e){
    this.password = e.target.value;
  }
  setUser(e){
    this.user = e.target.value;
  }
  setTipoDocu(e){
    this.documento = e.target.value;
  }
  usuario(){
      this.shadowRoot.getElementById("usuario").classList.toggle('hidden');
  }
  login(){
    let user={
      userId: this.documento + this.user,
      password: this.password
    }

    this.dispatchEvent(new CustomEvent('login', {
      detail: user,
      bubbles: true,
      composed: true,
    }));
  }
}
